package cs425.mp3.Messages;

public enum MessageType {
    Join,
    Leave,
    Crash,
    Ping,
    Ack,
    MemberListRequest,
    IntroducerCheckAlive,

    Metadata,

    GetReplicasToPut,
    ReturnPutReplicas,
    File,
    AckFile,

    GetReplicasToGet,
    ReturnGetReplicas,
    RequestFile,

    RequestDelete,
    DeleteFile,

    RequestVersion,
    RequestReplicate,

    FileOpPut,
    FileOpGet,
    FileOpDelete,
    FileOpGetVersion,
    FileOpStore,


    // Bully Message Type
    ELECTION,
    ELECTION_RESPONSE,
    VICTORY
}
