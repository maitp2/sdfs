package cs425.mp3.Messages;

import cs425.mp3.MembershipList.MemberListEntry;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Communication message. Contains message type and the member details
 */
public class Message implements Serializable {

    private MessageType messageType;
    private MemberListEntry subjectEntry;
    private String srcFileName;
    private String desFileName;
    private ArrayList<MemberListEntry> replicas;
    private byte[] content;
    private int versions;

    public Message(MessageType messageType, MemberListEntry subjectEntry) {
        this.messageType = messageType;
        this.subjectEntry = subjectEntry;
    }

    public Message(MessageType messageType, String srcFileName, String desFileName, ArrayList<MemberListEntry> replicas) {
        this.messageType = messageType;
        this.srcFileName = srcFileName;
        this.desFileName = desFileName;
        this.replicas = replicas;
    }

    public Message(MessageType messageType, int versions, String srcFileName, byte[] content, String desFileName) {
        this.srcFileName = srcFileName;
        this.versions = versions;
        this.messageType = messageType;
        this.content = content;
        this.desFileName = desFileName;
    }

    public Message(MessageType messageType, String desFileName, MemberListEntry subjectEntry) {
        this.messageType = messageType;
        this.desFileName = desFileName;
        this.subjectEntry = subjectEntry;
    }

    public int getVersions() {
        return versions;
    }

    public byte[] getContent() {
        return content;
    }

    public String getSrcFileName() {
        return srcFileName;
    }

    public String getDesFileName() {
        return desFileName;
    }

    public ArrayList<MemberListEntry> getReplicas() {
        return replicas;
    }

    public void setReplicas(ArrayList<MemberListEntry> replicas) {
        this.replicas = replicas;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public MemberListEntry getSubjectEntry() {
        return subjectEntry;
    }
}
