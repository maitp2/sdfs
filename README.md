# CS425_MP3

### Mai Pham (maitp2) and Trang Do(trangtd2)

## Instructions

### Run Introducer
```
java -jar out/artifacts/sdfs_jar/sdfs.jar <introducer_port>
```
Example:
```
java -jar out/artifacts/sdfs_jar/sdfs.jar 5000
```

### Run Master and Process
```
java -jar sdfs.jar 5001 <introducer_host> <introducer_port> <master_host> <master_port> 
```
Example
```
java -jar sdfs.jar 5001 fa22-cs425-5201.cs.illinois.edu 5000 fa22-cs425-5202.cs.illinois.edu 5001
```

### Note
After running process (except Introducer), run ```join``` to join membership
1. ``` join```: to join membership
2. ```leave```: to leave membership
3. ```list_mem```: to print all member
4. ```list_self```: to print process id
5. ```put <full_path_local_file> <file_name>```: put local file to sdfs with file_name. For example, 
```put /home/maitp2/file file1.txt <file_name>```
6. ```get <file_name_in_sdfs> <local_file_name>```: get sdfs file and saved to local file name
7. ```delete <file_in_sdfs>```: delete a sdfs file 
8. ```ls <sdfs_file_name>```: list all VM has this file in sdfs  
9. ```store```: list all files this querying VM has
10. ```get-verions <sdfs_file_name> <an integer num version> <localfile_to_save>```: get all versions of sdfs file and save them in local file

