upperlim=10

for ((i=1; i<=upperlim; i++)); do
        echo "Sending jar file to VM $i"

        if [ "$i" -lt 10 ]; then
          scp -r out/artifacts/sdfs_jar/sdfs.jar maitp2@fa22-cs425-520$i.cs.illinois.edu:/home/maitp2/
          ssh maitp2@fa22-cs425-520$i.cs.illinois.edu 'mkdir ./mp3_logs/'
          ssh maitp2@fa22-cs425-520$i.cs.illinois.edu 'rm -r ./sdfs_files_*'

        else
          scp -r out/artifacts/sdfs_jar/sdfs.jar maitp2@fa22-cs425-52$i.cs.illinois.edu:/home/maitp2/
          ssh maitp2@fa22-cs425-52$i.cs.illinois.edu 'mkdir ./mp3_logs/'
          ssh maitp2@fa22-cs425-520$i.cs.illinois.edu 'rm -r ./sdfs_files_*'
        fi
done
